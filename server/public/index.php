<?php
    session_start();

    if (is_null($_SESSION['deliveryTypes'])) {
        $_SESSION['deliveryTypes'] = [
            'Department',
            'Courier',
            'Parcel machine'
        ];
    }

    // filling the 'idProducts' array with random numbers without repetitions
    if (is_null($_SESSION['idProducts'])) {
        $_SESSION['idProducts'][0] = rand(1, 1000);
        for ($i = 1, $j; $i < 10; $i++) {
            while (1) {
                $randNum = rand(1, 1000);
                for ($j = 0; $j < $i; $j++) {
                    if ($randNum == $_SESSION['idProducts'][$j]) {
                        break;
                    }
                }
                if ($j == $i) {
                    break;
                }
            }
            $_SESSION['idProducts'][$i] = $randNum;
        }
    }

    /**
     * @param array<string> $resultParams
     * @param string $submissionResult
     */
    function showMessageSubmissionResult(array $resultParams, string $submissionResult = 'Your data has been sent successfully!'): void
    {
        echo "<div class='submitted-info'>";
        echo "<p>$submissionResult</p>";
        foreach($resultParams as $value) {
            echo "<div class='tub'>$value</div>";
        }
        echo "</div>";
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css">
        <title>Homework 4</title>
    </head>
    <body>
        <b class="task-caption">**Task 1:</b>
        <!--
            Создать форму оставить отзыв:
            a) поле имя
            b) поле комментарий
        -->
        <form class="form" action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post">
            <b class="form-caption">Your feedback</b>
            <div class="input-and-caption">
                <label for="feedback-username">Your name</label>
                <input id="feedback-username" type="text" name="username" placeholder="Enter your name" required>
            </div>
            <div class="input-and-caption">
                <label for="feedback-comment">Your comment</label>
                <textarea id="feedback-comment" name="comment" rows="5" placeholder="Enter your comment" required></textarea>
            </div>
            <div class="buttons">
                <input type="submit" value="Send">
                <input type="reset" value="Clear">
            </div>
        </form>

        <?php
            $submitUsername = $_POST['username'];
            $submitComment = $_POST['comment'];
            if(!is_null($submitUsername) && !is_null($submitComment)) {
                showMessageSubmissionResult([
                    "Username: $submitUsername",
                    "Comment: $submitComment"
                ]);
            }
        ?>

        <b class="task-caption">**Task 2:</b>
        <!--
            Создать форму добавить товар в корзину:
            a) id товара
            b) количество
        -->
        <form class="form" action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post">
            <b class="form-caption">Add item to cart</b>
            <div class="input-and-caption">
                <label>Product ID</label>
                <select name="cartIdProduct" required>

                    <?php
                        $idProducts = $_SESSION['idProducts'];
                        foreach ($idProducts as $key => $value) {
                            echo "<option value='$key'>$value</option>";
                        }
                    ?>

                </select>
            </div>
            <div class="input-and-caption">
                <label for="cart-num-product">Number of items</label>
                <input id="cart-num-product" type="number" name="numProduct" placeholder="Enter the number of product units" min='1' required>
            </div>
            <div class="buttons">
                <input type="submit" value="Send">
                <input type="reset" value="Clear">
            </div>
        </form>

        <?php
            $submitIdProduct = $_POST['cartIdProduct'];
            $submitNumProduct = $_POST['numProduct'];
            if(!is_null($submitNumProduct) && !is_null($submitNumProduct)) {
                showMessageSubmissionResult([
                    "ID product: $idProducts[$submitIdProduct]",
                    "The number of product units: $submitNumProduct"
                ]);
            }
        ?>

        <b class="task-caption">**Task 3:</b>
        <!--
            Обратная связь перезвонить клиенту:
            a) имя
            b) телефон
        -->
        <form class="form" action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post">
            <b class="form-caption">We will call you back!</b>
            <div class="input-and-caption">
                <label for="callback-name">Your name</label>
                <input id="callback-name" type="text" name="name" placeholder="Enter your name" required>
            </div>
            <div class="input-and-caption">
                <label for="callback-telephone-number">Your telephone number</label>
                <input id="callback-telephone-number" type="text" name="telNumber" placeholder="Enter your telephone number" required>
            </div>
            <div class="buttons">
                <input type="submit" value="Send">
                <input type="reset" value="Clear">
            </div>
        </form>

        <?php
            $submitName = $_POST['name'];
            $submitTelNumber = $_POST['telNumber'];
            if(!is_null($submitName) && !is_null($submitTelNumber)) {
                showMessageSubmissionResult([
                    "Name: $submitName",
                    "Telephone number: $submitTelNumber"
                ]);
            }
        ?>

        <hr>
        <b class="task-caption">***Task 1:</b>
        <!--
            Создать форму оформить заказ:
            a) имя
            b) телефон
            c) тип доставки
            d) адрес доставки
            e) сессия получить список id товаров и передать в форму
        -->
        <form class="form" action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post">
            <b class="form-caption">Place an order</b>
            <div class="input-and-caption">
                <label for="order-username">Your name</label>
                <input id="order-username" type="text" name="orderUsername" placeholder="Enter your name" required>
            </div>
            <div class="input-and-caption">
                <label for="order-tel-number">Your telephone number</label>
                <input id="order-tel-number" type="text" name="orderTelNumber" placeholder="Enter your telephone number" required>
            </div>
            <div class="input-and-caption">
                <label>Delivery type</label>
                <select name="deliveryType" required>

                <?php
                    $deliveryTypes = $_SESSION['deliveryTypes'];
                    foreach ($deliveryTypes as $key => $value) {
                        echo "<option value='$key'>$value</option>";
                    }
                ?>

                </select>
            </div>
            <div class="input-and-caption">
                <label for="order-delivery-address">Delivery address</label>
                <input id="order-delivery-address" type="text" name="deliveryAddress" placeholder="Enter deleivery address" required>
            </div>
            <div class="input-and-caption">
                <label>Product ID</label>
                <select name="orderIdProduct" required>

                    <?php
                        foreach ($idProducts as $key => $value) {
                            echo "<option value='$key'>$value</option>";
                        }
                    ?>

                </select>
            </div>
            <div class="buttons">
                <input type="submit" value="Send">
                <input type="reset" value="Clear">
            </div>
        </form>

        <?php
            $submitName = $_POST['orderUsername'];
            $submitTelNumber = $_POST['orderTelNumber'];
            $submitDeliveryType = $_POST['deliveryType'];
            $submitDeliveryAddress = $_POST['deliveryAddress'];
            $submitIdProduct = $_POST['orderIdProduct'];
            if (!is_null($submitName) && !is_null($submitTelNumber) && !is_null($submitDeliveryType) && !is_null($submitDeliveryAddress) && !is_null($submitIdProduct)) {
                showMessageSubmissionResult([
                    "Name: $submitName",
                    "Telephone number: $submitTelNumber",
                    "Delivery type: $deliveryTypes[$submitDeliveryType]",
                    "Delivery address: $submitDeliveryAddress",
                    "ID product: $idProducts[$submitIdProduct]"
                ]);
            }
        ?>
        <hr>
        <b class="task-caption">****Task 1:</b>
        <!--
            Загружать файлы на сервер, попробуйте разрешить загрузку файлов только определенного формата
            a) pdf
            b) excel
            c) word
            для изображений реализуй
            d) png
            e)jpg
        -->
        <form class="form" enctype="multipart/form-data" action="<?= $_SERVER['SCRIPT_NAME'] ?>" method="post">
            <b class="form-caption">Select a file to upload to the server</b>
            <input type="hidden" name="maxFileSize" value="5000000">
            <div class="input-and-caption">
                <input id="input-file" class="input" name="userfile" type="file" required>
            </div>
            <div class="buttons">
                <input type="submit" value="Send">
                <input type="reset" value="Clear">
            </div>
        </form>
        <?php
            if(isset($_FILES['userfile'])) {
                $serverFileName = $_FILES['userfile']['tmp_name'];
                if(is_uploaded_file($serverFileName)) {
                    $fileName = $_FILES['userfile']['name'];
                    $fileFormat = $_FILES['userfile']['type'];
                    $fileSize = $_FILES['userfile']['size'];
                    $maxFileSize = $_POST['maxFileSize'];
                    $validFormats = [
                        'application/pdf'                                                         => 'pdf',
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'       => 'xlsx',
                        'application/vnd.ms-excel'                                                => 'xls',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
                        'application/msword'                                                      => 'doc',
                        'image/png'                                                               => 'png',
                        'image/jpeg'                                                              => 'jpeg'
                    ];
                    $errors = [];
                    if(!array_key_exists($fileFormat, $validFormats)) {
                        $stringValidFormats = implode(', ', $validFormats);
                        $errors[] = "Supported formats: $stringValidFormats";
                    }
                    if($fileSize > $maxFileSize) {
                        $errors[] = "Max file size: $maxFileSize bytes";
                    }
                    if(count($errors) > 0) {
                        showMessageSubmissionResult($errors, 'The file cannot be saved to the server!');
                    } else {
                        move_uploaded_file($serverFileName, $_SERVER['DOCUMENT_ROOT']."/loadFiles/$fileName");
                        showMessageSubmissionResult(
                            [
                                "File name: $fileName",
                                "File size: $fileSize bytes",
                                "File format: $validFormats[$fileFormat]"
                            ],
                            'The file has been successfully saved on the server!'
                        );
                    }
                }    
            }
        ?>
    </body>
</html>